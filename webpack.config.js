var debug = process.env.NODE_ENV !== "production";
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack = require('webpack');
var path = require('path');

module.exports = {
	context: path.join(__dirname, "src"),
	entry: './js/app.js',
	output: {
		publicPath: 'http://localhost:3000/',
		filename: "src/app.min.js"
	},
	devtool: 'eval',
	module: {
		loaders: [
			{
				test: /\.scss$/,
				exclude: /node_modules/,
      			loaders: ['style-loader', 'css-loader', 'sass-loader'],
			},
			{
				test: /.js?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2016']
				}
			}

		]
	},
	plugins: [
		new ExtractTextPlugin("src/app.css")
	]
};

