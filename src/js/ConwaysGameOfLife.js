const defaultConfig = {
	gridWidth: 250,
	gridHeight: 250,
	cellSize: 2,
	interval: 100
}

const DEAD = 0;
const ALIVE = 1

const ConwaysGameOfLife = function(_config={}) {
	const callbacks = [];
	const config = Object.assign({},defaultConfig,_config);
	let data = createDeadData();

	function createDeadData() {
		return Array(config.gridWidth).fill().map(arr=>Array(config.gridHeight).fill(DEAD));
	}

	function randomiseData() {
		data = data.map(col=>col.map(cell=>Math.floor(Math.random()*2)>0?ALIVE:DEAD))
	}

	function start() {
		randomiseData();
		triggerCallbacks();
		setInterval(tick,config.interval);
	}

	function addCallback(callback) {
		callbacks.push(callback);
	}

	function triggerCallbacks() {		
		for (let i in callbacks) {
			callbacks[i].call();
		}
	}

	function tick() {
		let newData = createDeadData();
		
		for (let x=0;x<config.gridWidth;x++) {
			for (let y=0;y<config.gridHeight;y++) {
				newData[x][y] = getNewStateOfCell({x,y})
			}
		}
		data = newData;
		triggerCallbacks();
	}

	function isCellInRange({x,y}) {
		return x>=0 && y>=0 && x<config.gridWidth && y<config.gridHeight
	}

	function getLiveNeighboursForCell({x,y}) {
		let count = 0;
		for (let w =-1;w<=1;w++) {
			for (let h =-1;h<=1;h++) {
				count += !(w===0&&h===0) && isCellInRange({x:x+w,y:y+h}) && data[x+w][y+h] === ALIVE ? 1 : 0
			}
		}
		return count;
	}

	function isCellAlive({x,y}) {
		return data[x][y] === ALIVE
	}

	function getNewStateOfCell({x,y}) {
		const liveNeighboursCount = getLiveNeighboursForCell({x,y})
		const isAlive = isCellAlive({x,y})
		if (isAlive) {
			return (liveNeighboursCount<2 || liveNeighboursCount>3)? DEAD : ALIVE
		} else {
			return liveNeighboursCount === 3? ALIVE : DEAD
		}
	}


	this.getConfig = ()=>config
	this.getData = ()=>data
	this.getLiveNeighboursForCell = getLiveNeighboursForCell
	this.isCellAlive = isCellAlive;
	this.onTick = (callback) => addCallback(callback)
	this.start = start
}

export default ConwaysGameOfLife;