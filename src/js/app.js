import Game from './ConwaysGameOfLife'
require('../sass/styles.scss')


const myGame = new Game()
const gameConfig = myGame.getConfig({cellSize:5});
window.myGame = myGame


const canvas = document.getElementById('canvas')
const context = canvas.getContext('2d')
const canvasWidth = 500;
const canvasHeight = 500;


function clearCanvas() {
	context.width = 501
	context.height = 501
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.fillStyle="#EA5C55";
}

function drawCells() {
	for (let x=0;x<gameConfig.gridWidth;x++) {
		for (let y=0;y<gameConfig.gridHeight;y++) {
			if (myGame.isCellAlive({x,y})) {
				context.fillRect(x*gameConfig.cellSize,y*gameConfig.cellSize,gameConfig.cellSize,gameConfig.cellSize)
			}
		}
	}
}

function drawCanvas () {
	clearCanvas();
	drawCells();
}

myGame.onTick(drawCanvas)
myGame.start();
